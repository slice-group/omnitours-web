class CreateKepplerCatalogsAttachments < ActiveRecord::Migration
  def change
    create_table :keppler_catalogs_attachments do |t|
      t.string :name
      t.belongs_to :category
      t.string :price
      t.string :number_of_days
      t.string :date_image
      t.text :url
      t.text :map
      t.text :description
      t.text :include_plan
      t.text :no_include_plan
      t.text :notes
      t.text :itinerary
      t.boolean :public
      t.string :permalink
      t.belongs_to :catalog

      t.timestamps null: false
    end
  end
end
