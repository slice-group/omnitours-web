class CreateKepplerCatalogsAttachmentImages < ActiveRecord::Migration
  def change
    create_table :keppler_catalogs_attachment_images do |t|
      t.integer :attachment_id
      t.string :image

      t.timestamps null: false
    end
  end
end
