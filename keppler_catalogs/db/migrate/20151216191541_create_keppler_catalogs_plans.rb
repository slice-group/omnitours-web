class CreateKepplerCatalogsPlans < ActiveRecord::Migration
  def change
    create_table :keppler_catalogs_plans do |t|
      t.integer :attachment_id
      t.string :name
      t.string :url

      t.timestamps null: false
    end
  end
end
