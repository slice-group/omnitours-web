module KepplerCatalogs
  class AttachmentImage < ActiveRecord::Base
  	mount_uploader :image, ImageUploader
  	belongs_to :attachment
  end
end
