#Generado por keppler
require 'elasticsearch/model'
module KepplerCatalogs
  class Catalog < ActiveRecord::Base
    include Elasticsearch::Model
    include Elasticsearch::Model::Callbacks
    mount_uploader :cover, CoverUploader
    before_save :create_permalink
    has_many :attachments, :dependent => :destroy
    validates_presence_of :title, :section    
    validates_length_of :title, :minimum => 1, :maximum => 40

    after_commit on: [:update] do
      puts __elasticsearch__.index_document
    end

    def attachments_publised
      self.attachments.where(public:true)
    end
    
    def self.searching(query)
      if query
        self.search(self.query query).records.order(id: :desc)
      else
        self.order(id: :desc)
      end
    end

    def self.query(query)
      { query: { multi_match:  { query: query, fields: [:title, :description, :section, :public] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
    end

    def as_indexed_json(options={})
      {
        id: self.id.to_s,
        title:  self.title.to_s,
        description: ActionView::Base.full_sanitizer.sanitize(self.description, tags: []),
        section:  self.section,
        public:  self.public.to_s ? "publicado": "no--publicado"
      }.as_json
    end
    
    def published_files
      self.attachments.where(public: true)
    end

    def published_files_count
      self.attachments.where(public: true).count
    end

    private

    def create_permalink
      self.permalink = self.title.downcase.parameterize
    end

  end
  #Catalog.import
end
