#Generado por keppler
require 'elasticsearch/model'
module KepplerCatalogs
  class Attachment < ActiveRecord::Base
    include Elasticsearch::Model
    include Elasticsearch::Model::Callbacks
    belongs_to :catalog
    belongs_to :category
    before_save :create_permalink, :if => :new_record?
    has_many :attachment_images, :dependent => :destroy
    has_many :plans, :dependent => :destroy
    accepts_nested_attributes_for :attachment_images, :allow_destroy => true, :reject_if => :all_blank
    accepts_nested_attributes_for :plans, :allow_destroy => true, :reject_if => :all_blank
    validates_presence_of :name, :subtitle, :category
    mount_uploader :date_image, ImageUploader
    validates_presence_of :attachment_images

    
    after_commit on: [:update] do
      __elasticsearch__.index_document
    end



    def self.searching(query)
      if query
        self.search(self.query query).records.order(id: :desc)
      else
        self.order(id: :desc)
      end
    end

    def self.query(query)
      { query: { multi_match:  { query: query, fields: [:name, :description, :category, :public] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
    end

    #armar indexado de elasticserch
    def as_indexed_json(options={})
      {
        id: self.id.to_s,
        name:  self.name,
        description: ActionView::Base.full_sanitizer.sanitize(self.description, tags: []),
        category: self.category.name,
        public:  self.public ? "publicado" : "no--publicado"
      }.as_json
    end

    def create_permalink
      self.permalink = "#{self.name.downcase.parameterize}-#{SecureRandom.hex(3)}"
    end

  end
  #Attachment.import
end
