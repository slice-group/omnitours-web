window.SocialShareButton =
  openUrl : (url,popup) ->
    if popup == "true"
      left = (screen.width/2)-(600/2)
      top = (screen.height/2)-(400/2)
      window.open(url,'popup','height=400,width=600,top='+top+', left='+left+'')
    else
      window.open(url)
      false

  share : (el) ->
    site = $(el).data('site')
    appkey = $(el).data('appkey') || ''
    $parent = $(el).parent()
    title = encodeURIComponent($(el).data(site + '-title') || $parent.data('title') || '')
    img = encodeURIComponent($parent.data("img") || '')
    url = encodeURIComponent($parent.data("url") || '')
    via = encodeURIComponent($parent.data("via") || 'Omnitours')
    desc = encodeURIComponent($parent.data("desc") || ' ')
    popup = encodeURIComponent($parent.data("popup") || 'true')

    if url.length == 0
      url = encodeURIComponent(location.href)
    switch site
      when "twitter"
        via_str = ''
        via_str = "&via=#{via}" if via.length > 0
        SocialShareButton.openUrl("https://twitter.com/intent/tweet?url=#{url}&text=#{title}#{via_str}", popup)
      when "facebook"
        SocialShareButton.openUrl("http://www.facebook.com/sharer.php?u=#{url}",popup)
      when "google_plus"
        SocialShareButton.openUrl("https://plus.google.com/share?url=#{url}", popup)
    false