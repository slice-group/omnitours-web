#= require jquery
#= require jquery.turbolinks
#= require jquery_ujs
#= require angular
#= require sidebar
#= require noty
#= require checklist-model
#= require angular-local-storage
#= require materialize-sprockets
#= require nprogress
#= require nprogress-turbolinks
#= require keppler_catalogs/application
#= require cocoon
#= require ckeditor/init
#= require turbolinks
#= require_tree .
		
