class FrontendController < ApplicationController
	layout 'layouts/frontend/application'
	
  def index
  	@catalogs = KepplerCatalogs::Catalog.where(public:true).order('id DESC')
  end

  def package_show
  	@attachment = KepplerCatalogs::Catalog.find_by_permalink(params[:catalog_permalink]).attachments.find_by_permalink(params[:attachment_permalink])
  	@plans = KepplerCatalogs::Plan.find_by_attachment_id(@attachment.id)
  end
end