class AddSubtitleToKepplerCatalogsAttachments < ActiveRecord::Migration
  def change
    add_column :keppler_catalogs_attachments, :subtitle, :string
  end
end
